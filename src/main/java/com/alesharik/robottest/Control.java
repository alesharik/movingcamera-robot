package com.alesharik.robottest;

import ev3dev.actuators.lego.motors.EV3LargeRegulatedMotor;
import ev3dev.robotics.tts.Espeak;
import ev3dev.sensors.Button;
import ev3dev.sensors.ev3.EV3UltrasonicSensor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class Control {
//    public static final EV3UltrasonicSensor ULTRASONIC_SENSOR = new EV3UltrasonicSensor(SensorPort.S3);
    public static final EV3LargeRegulatedMotor RIGHT_MOTOR = new EV3LargeRegulatedMotor(MotorPort.C);
    public static final EV3LargeRegulatedMotor LEFT_MOTOR = new EV3LargeRegulatedMotor(MotorPort.B);
    public static final EV3LargeRegulatedMotor STABIL_Z = new EV3LargeRegulatedMotor(MotorPort.D);
    public static final EV3LargeRegulatedMotor STABIL_Y = new EV3LargeRegulatedMotor(MotorPort.A);
    public static final AtomicBoolean canMoveForward = new AtomicBoolean(true);
    private static final AtomicInteger rotationZ = new AtomicInteger();
    private static final AtomicInteger rotationY = new AtomicInteger();
    private static final CameraMover mover = new CameraMover();
    public static void start() {
//        new DistanceChecker().start();
        mover.start();
//        STABIL_Z.setSpeed((int) (STABIL_Z.getMaxSpeed() / 5));
//        STABIL_Y.setSpeed((int) (STABIL_Y.getMaxSpeed() / 5));
        Runtime.getRuntime().addShutdownHook(new ShutdownThread());
    }

    public static void rotateLeftWheel(int speedPercent) {
        LEFT_MOTOR.setSpeed((int) (LEFT_MOTOR.getMaxSpeed() / 100F * Math.abs(speedPercent)));
        if(speedPercent == 0)
            LEFT_MOTOR.stop();
        else if(speedPercent > 0)
            LEFT_MOTOR.forward();
        else if(speedPercent < 0)
            LEFT_MOTOR.backward();
    }

    public static void rotateRightWheel(int speedPercent) {
        RIGHT_MOTOR.setSpeed((int) (LEFT_MOTOR.getMaxSpeed() / 100F * Math.abs(speedPercent)));
        if(speedPercent == 0)
            RIGHT_MOTOR.stop();
        else if(speedPercent > 0)
            RIGHT_MOTOR.forward();
        else if(speedPercent < 0)
            RIGHT_MOTOR.backward();
    }

    public static void brakeWheels() {
        LEFT_MOTOR.brake();
        RIGHT_MOTOR.brake();
    }

    public static void setFloating(boolean is) {
        LEFT_MOTOR.flt(is);
        RIGHT_MOTOR.flt(is);
        STABIL_Y.setSpeed((int) STABIL_Y.getMaxSpeed());
        STABIL_Z.setSpeed((int) STABIL_Z.getMaxSpeed());
    }

    public static void moveCamera(CameraMoveType type) {
        mover.setMove(type);
    }

    private static final class ShutdownThread extends Thread {
        @Override
        public void run() {
            RIGHT_MOTOR.stop();
            LEFT_MOTOR.stop();
            STABIL_Z.rotate(rotationZ.get() * -1);
            STABIL_Y.rotate(rotationY.get() * -1);
        }
    }

    public enum CameraMoveType {
        NO,
        LEFT,
        RIGHT,
        UP,
        DOWN
    }

    private static final class CameraMover extends Thread {
        private static final int MOVE_TIME = 10;
        private volatile CameraMoveType move;
        private volatile CameraMoveType current;
        private volatile int ms = 0;

        @Override
        public void run() {
            while (true) {
                try {
                    if (ms > 0) {
                        ms--;
                        current = move;
                    } else
                        current = CameraMoveType.NO;
                    if (current == CameraMoveType.NO) {
                        STABIL_Z.stop();
                        STABIL_Y.stop();
                    } else if (current == CameraMoveType.LEFT) {
                        STABIL_Z.forward();
                    } else if (current == CameraMoveType.RIGHT) {
                        STABIL_Z.backward();
                    } else if (current == CameraMoveType.UP) {
                        STABIL_Y.forward();
                    } else if (current == CameraMoveType.DOWN) {
                        STABIL_Y.backward();
                    }
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        public void setMove(CameraMoveType type) {
            move = type;
            ms = MOVE_TIME;
        }
    }
//
//    private static final class DistanceChecker extends Thread {
//        public DistanceChecker() {
//            setDaemon(true);
//        }
//
//        @Override
//        public void run() {
//            System.out.println("DistanceChecker started");
//            RIGHT_MOTOR.setSpeed((int) RIGHT_MOTOR.getMaxSpeed());
//            LEFT_MOTOR.setSpeed((int) LEFT_MOTOR.getMaxSpeed());
//            ULTRASONIC_SENSOR.enable();
//            while (isAlive() && !isInterrupted()) {
//                float[] values = new float[ULTRASONIC_SENSOR.getDistanceMode().sampleSize()];
//                ULTRASONIC_SENSOR.getDistanceMode().fetchSample(values, 0);
//                boolean ok = true;
//                for (float value : values) {
//                    if(value < 12) {
//                        RIGHT_MOTOR.stop();
//                        LEFT_MOTOR.stop();
//                        ok = false;
//                    }
//                }
//                canMoveForward.set(ok);
//                try {
//                    Thread.sleep(100);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//            ULTRASONIC_SENSOR.disable();
//        }
//    }
}
