# MovingCamera Robot App
This app is run on a robot

| Port     | Target                | Description                        |
|----------|-----------------------|------------------------------------|
| Motor.C  | Right move motor      |                                    |
| Motor.B  | Left move motor       |                                    |
| Motor.D  | Z axis stabilizator   | Motor that rotate camera by Z axis |
| Motor.A  | Y axis stabilizator   | Motor that rotate camera by Y axis |
| Sensor.3 | Front distance sensor |                                    |